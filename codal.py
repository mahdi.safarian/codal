import requests
import pandas as pd
import json
from time import sleep

link = 'https://search.codal.ir/api/search/v2/q?&Audited=true&AuditorRef=-1&Category=1&Childs=true&CompanyState=0&CompanyType=-1&Consolidatable=true&IsNotAudited=false&Length=-1&LetterType=6&Mains=true&NotAudited=true&NotConsolidatable=true&Publisher=false&TracingNo=-1&search=true&PageNumber='
items = []
Years = ['1394','1395','1396','1397','1398']
Months =   ['03','06','09','12']
LastDays = ['31','31','30','29']

for years in range(len(Years)):
    for months in range(len(Months)):
        if Years[years] == '1395' and Months[months] == '12':
            YearsEndTo = Years[years]+'%2F'+Months[months]+'%2F30'
            print(YearsEndTo)
        else:
            YearsEndTo = Years[years]+'%2F'+Months[months]+'%2F'+LastDays[months]
            print(YearsEndTo)
        print('****************************************')
        sleep(1.5)
        r = requests.get(link+'1'+'&YearEndToDate='+YearsEndTo,verify=False)
        data = r.json()
        page = data['Page']
        print('Pages:',page)
        for i in range(1,page+1):
            print('Page:',i)
            print(link+str(i)+'&YearEndToDate='+YearsEndTo)
            sleep(1.5)
            r = requests.get(link+str(i)+'&YearEndToDate='+YearsEndTo,verify=False)
            data = r.json()
            for k in range(len(data["Letters"])):
                items.append((data['Letters'][k]['Symbol'],data['Letters'][k]['CompanyName'],data['Letters'][k]['SentDateTime'][0:10],data['Letters'][k]['PublishDateTime'][0:10],data['Letters'][k]['Title']))
codal = pd.DataFrame(items, columns=['نماد','نام شرکت','زمان ارسال','زمان انتشار','عنوان اطلاعیه'])  
codal.reset_index(drop=True, inplace=True)
codal.to_csv('codal.csv',encoding='utf-8-sig')
print("finished",total)